package Demo.amadeus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVReader;

public class App 
{
	static WebDriver driver;
	static CSVReader read=null;
	static String fecha="";  
   
    
    public static void iniciarDriver() {
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ivan\\eclipse-workspace\\Demoid\\src\\main\\java\\Demo\\Demoid\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS) ;
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-hhmm");
        Date date = new Date();
        fecha=dateFormat.format(date);
    }
    
    public static void iniciarURL(String url) {
    	driver.get("file:///C:/Users/ivan/Desktop/demoHTML.html");
    }
    
    
    public static void llenarFormulario(String textos,String areaTextos,String passwords,String fruta,String pais) {
    	WebDriverWait wait=new WebDriverWait(driver, 60);
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/button")));
    	WebElement boton=driver.findElement(By.xpath("/html/body/button"));
    	WebElement texto=driver.findElement(By.xpath("//*[@name=\"mitexto\"]"));
    	WebElement areaTexto=driver.findElement(By.xpath("//*[contains(text(), 'TEXT BOX Y ESTOY')]"));
    	WebElement password=driver.findElement(By.xpath("//*[@type=\"password\"]"));
    	WebElement radioB=driver.findElement(By.xpath("//*[@value=\"radio1\"]"));
    	Select select=new Select(driver.findElement(By.xpath("/html/body/select[1]")));
    	Select select2=new Select(driver.findElement(By.xpath("/html/body/select[2]")));
    	WebElement date=driver.findElement(By.xpath("/html/body/input[5]"));
    	boton.click();
    	
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
    	
    	Alert alert = driver.switchTo().alert();
    	alert.accept();
    	texto.sendKeys(textos);
    	areaTexto.clear();
    	areaTexto.sendKeys(areaTextos);
    	password.sendKeys(passwords);
    	radioB.click();
    	select.selectByVisibleText(fruta);
    	select2.selectByVisibleText(pais);
    	date.sendKeys("01-30-2015");
    }
    
    public static void cerrarNavegador() {
    	driver.quit();
    }
    
    public static void screenshot(String descripcion) {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			//           logs/20190412-1143/archivo.png
			FileUtils.copyFile(scrFile, new File("logs/"+fecha+"/"+descripcion+".png"));
		} catch (IOException e) {
			System.err.println("No se pudo tomar el pantallazo "+e.getMessage());
		}
	}
}
