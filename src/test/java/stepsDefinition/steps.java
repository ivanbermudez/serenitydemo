package stepsDefinition;


import Demo.amadeus.App;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class steps {
	@Given("^imprimir en consola (.*)$")
	public void imprimir_en_consola(String variable) throws Throwable {
	    System.out.println("hola "+variable);
	}
	
	@Then("^llena el formulario con texto: (.*), areatexto: (.*), password: (.*), fruta: (.*), pais: (.*)$")
	public void llenarFormulario(String texto,String areaTexto, String password, String fruta, String pais) {
		App.llenarFormulario(texto, areaTexto, password, fruta, pais);
	}
	
	@And("^abrir url (.*)$")
	public void abrirURL(String url) {
		App.iniciarURL(url);
	}
	
	@And("^iniciar driver$")
	public void inicializarDriver() {
		App.iniciarDriver();
	}
	
	@And("^cerrar navegador$")
	public void cerrarBrowser(){
		App.cerrarNavegador();
	}
}
