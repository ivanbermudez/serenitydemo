package Demo.amadeus;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import net.serenitybdd.cucumber.CucumberWithSerenity; 

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/java/features", glue="stepsDefinition")

public class testRunner {

}
